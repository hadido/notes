from django.shortcuts import render, redirect
from notes.models import Category, Note
from django.views.decorators.csrf import csrf_protect


def index_view(request):
    categories = Category.objects.all()
    last_notes = Note.objects.all().reverse()[:10]
    return render(request, "notes/index.html", {'categories': categories, 'notes': last_notes})


def category_view(request, cid):
    categories = Category.objects.all()
    cat_title = categories.get(id=cid).title
    notes = Note.objects.filter(category_id=cid)
    return render(request, "notes/category_view.html", dict(categories=categories, notes=notes, cat_title=cat_title))


@csrf_protect
def search_view(request):
    if request.POST and request.POST['search'] != '':
        notes = Note.objects.filter(content__contains=request.POST['search']) or Note.objects.filter(
            title__contains=request.POST['search'])
        return render(request, "notes/search.html",
                      dict(categories=Category.objects.all(), notes=notes, my_search=request.POST['search']))
    else:
        return redirect('/')
