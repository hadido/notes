from django.contrib import admin

from notes.models import Note, Category


class NoteAdmin(admin.ModelAdmin):
    list_display = ['title']
    list_filter = ['']

admin.site.register(Category)
admin.site.register(Note)
