import os

from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.utils.translation import ugettext as _


class Category(models.Model):
    title = models.CharField(max_length=64, unique=True)
    icon = models.ImageField(null=True, blank=True)

    def __str__(self):
        return '%s' % self.title

    class Meta:
        ordering = ['title']
        verbose_name = _('category')
        verbose_name_plural = _('categories')


@receiver(models.signals.post_delete, sender=Category)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.icon:
        if os.path.isfile(instance.icon.path):
            os.remove(instance.icon.path)


@receiver(models.signals.post_delete, sender=Category)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).icon
    except sender.DoesNotExist:
        return False

    # new_file = instance.picture
    if old_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class Note(models.Model):
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    title = models.CharField(max_length=64, unique=True)
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s - %s' % (self.title, self.category.title)

    class Meta:
        ordering = ['id']
        verbose_name = _('note')
        verbose_name_plural = _('notes')
