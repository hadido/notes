/**
 * Created by hadi on 5/19/18.
 */
  $( function() {
    var state = true;
    $( "#close" ).on( "click", function() {
      if ( state ) {
        $( "#sidenav" ).animate({
          width: 0
        }, 300 );
        $( "#main" ).animate({
          "margin-left": 0
        }, 300 );
        $( "#navbar" ).animate({
          "margin-left": 0
        }, 300 );
      } else {
        $( "#sidenav" ).animate({
          width: 250
        }, 300 );
        $( "#main" ).animate({
          "margin-left": 250
        }, 300 );
        $( "#navbar" ).animate({
          "margin-left": 250
        }, 300 );
      }
      state = !state;
    });
  } );
