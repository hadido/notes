
function myFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("div")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
            document.getElementById(String(i)+"note-content").style.visibility = "";
        } else {
            li[i].style.display = "none";
            document.getElementById(String(i)+"note-content").style.display = "none";
        }

    }
}